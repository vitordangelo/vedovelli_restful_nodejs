const db = require('../../services/mysql')

const routes = (server) => {
  server.post('auth', async (req, res, next) => {
    const user = req.body
    try {
      res.send(await db.auth().authenticate(user))
    } catch (error) {
      res.send(error)
    }
    next()
  })

  server.get('categoria', async (req, res, next) => {
    try {
      // res.send(await db.categories().all())
      const categories = await db.categories().all()
      const user = req.decoded
      res.send({ categories, user })
    } catch (error) {
      res.send(error)
    }
    next()
  })

  server.post('categoria', async (req, res, next) => {
    const category = req.body
    try {
      res.send(await db.categories().save(category))
    } catch (error) {
      res.send(error)
    }
    next()
  })

  server.put('categoria', async (req, res, next) => {
    const category = req.body
    try {
      res.send(await db.categories().update(category))
    } catch (error) {
      res.send(error)
    }
    next()
  })

  server.del('categoria', async (req, res, next) => {
    const category = req.body
    try {
      res.send(await db.categories().del(category))
    } catch (error) {
      res.send(error)
    }
    next()
  })
}

module.exports = routes
