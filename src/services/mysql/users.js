const sha1 = require('sha1')

const users = (deps) => {
  return {
    all: () => {
      return new Promise((resolve, reject) => {
        const { connection, errorHandler } = deps
        connection.query('SELECT id, email FROM users', (error, results) => {
          if (error) {
            errorHandler(error, 'Falha ao listar usuários', reject)
            return false
          }
          resolve({status: 200, users: results})
        })
      })
    },

    save: (user) => {
      return new Promise((resolve, reject) => {
        const { connection, errorHandler } = deps
        connection.query('INSERT INTO users (email, password) VALUES (?, ?)', [user.email, sha1(user.password)], (error, results) => {
          if (error) {
            errorHandler(error, `Falha ao inserir usuário ${user.email}`, reject)
            return false
          }
          resolve({ status: 200, id: results.insertId, user })
        })
      })
    },

    update: (user) => {
      return new Promise((resolve, reject) => {
        const { connection, errorHandler } = deps
        connection.query('UPDATE users SET password = ? WHERE id = ?', [sha1(user.password), user.id], (error, results) => {
          if (error) {
            errorHandler(error, `Falha ao atualizar usuário ${user.id}`, reject)
            return false
          }
          if (results.affectedRows === 0) {
            resolve({ status: 'ID não encontrado' })
          }
          resolve({ status: 200, results })
        })
      })
    },

    del: (user) => {
      return new Promise((resolve, reject) => {
        const { connection, errorHandler } = deps
        connection.query('DELETE FROM users WHERE id = ?', [user.id], (error, results) => {
          if (error) {
            errorHandler(error, `Falha ao remover usuário de id ${user.id}`, reject)
            return false
          }
          if (results.affectedRows === 0) {
            resolve({ status: 'ID não encontrado' })
          }
          resolve({ status: 200, results })
        })
      })
    }
  }
}

module.exports = users
