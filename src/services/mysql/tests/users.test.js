const test = require('ava')
const { connection, errorHandler } = require('./setup')

const users = require('../users')({ connection, errorHandler })

const create = () => users.save({'email': 'user@test.com', 'password': '123456'})

test.beforeEach(t => connection.query('TRUNCATE TABLE users'))
test.after.always(t => connection.query('TRUNCATE TABLE users'))

test('Criação de usuário', async t => {
  const result = await create()
  t.is(result.user.email, 'user@test.com')
})

test('Lista de usuários', async t => {
  await create()
  const list = await users.all()
  t.is(list.users.length, 1)
})

test('Atualização de usuário', async t => {
  await create()
  const updated = await users.update({ 'id': 1, 'password': '123456789' })
  t.is(updated.results.affectedRows, 1)
})

test('Remoção de usuário', async t => {
  await create()
  const removed = await users.del({ 'id': 1 })
  t.is(removed.results.affectedRows, 1)
})
