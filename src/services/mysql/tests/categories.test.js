const test = require('ava')
const { connection, errorHandler } = require('./setup')

const categories = require('../categories')({ connection, errorHandler })

const create = () => categories.save({'name': 'Teste'})

test.beforeEach(t => connection.query('TRUNCATE TABLE categories'))
test.after.always(t => connection.query('TRUNCATE TABLE categories'))

test('Criação de categoria', async t => {
  const result = await create()
  t.is(result.category.name, 'Teste')
})

test('Lista de categorias', async t => {
  await create()
  const list = await categories.all()
  t.is(list.categories.length, 1)
})

test('Atualização de categoria', async t => {
  await create()
  const updated = await categories.update({ 'id': 1, 'name': 'category-test-updated' })
  t.is(updated.category.name, 'category-test-updated')
})

test('Remoção de categoria', async t => {
  await create()
  const removed = await categories.del({ 'id': 1 })
  t.is(removed.results.affectedRows, 1)
})
