const categories = (deps) => {
  return {
    all: () => {
      return new Promise((resolve, reject) => {
        const { connection, errorHandler } = deps
        connection.query('SELECT * FROM categories', (error, results) => {
          if (error) {
            errorHandler(error, 'Falha ao listar categorias', reject)
            return false
          }
          resolve({status: 200, categories: results})
        })
      })
    },

    save: (category) => {
      return new Promise((resolve, reject) => {
        const { connection, errorHandler } = deps
        connection.query('INSERT INTO categories SET ?', category, (error, results) => {
          if (error) {
            errorHandler(error, `Falha ao inserir categoria ${category.name}`, reject)
            return false
          }
          resolve({ status: 200, id: results.insertId, category })
        })
      })
    },

    update: (category) => {
      return new Promise((resolve, reject) => {
        const { connection, errorHandler } = deps
        connection.query('UPDATE categories SET name = ? WHERE id = ?', [category.name, category.id], (error, results) => {
          if (error) {
            errorHandler(error, `Falha ao atualizar categoria ${category.name}`, reject)
            return false
          }
          if (results.affectedRows === 0) {
            resolve({ status: 'ID não encontrado' })
          }
          resolve({ status: 200, category })
        })
      })
    },

    del: (category) => {
      return new Promise((resolve, reject) => {
        const { connection, errorHandler } = deps
        connection.query('DELETE FROM categories WHERE id = ?', [category.id], (error, results) => {
          if (error) {
            errorHandler(error, `Falha ao remover categoria de id ${category.id}`, reject)
            return false
          }
          if (results.affectedRows === 0) {
            resolve({ status: 'ID não encontrado' })
          }
          resolve({ status: 200, results })
        })
      })
    }
  }
}

module.exports = categories
