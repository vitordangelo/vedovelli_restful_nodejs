const jwt = require('jsonwebtoken')

const jwtMiddleare = (deps) => {
  return async (req, res, next) => {
    if (!deps.routesExclusions.includes(req.href())) {
      const token = req.headers['x-access-token']

      if (!token) {
        res.send(403, { error: 'Token não fornecido' })
        return false
      }

      // await jwt.verify(token, process.env.JWT_SECRET, (error, decoded) => {
      //   if (error) {
      //     res.send(403, { error: 'Falha ao autenticar o token' })
      //   } else {
      //     req.decoded = decoded
      //   }
      // })

      try {
        req.decoded = jwt.verify(token, process.env.JWT_SECRET)
      } catch (error) {
        res.send(403, { error: 'Falha ao validar o token' })
        return false
      }
    }

    next()
  }
}

module.exports = jwtMiddleare
